package main

import (
	"fmt"
	"strings"
)

func main() {
	var menu int
	fmt.Println("Silahkan pilih menu penukaran uang")
	fmt.Println("1. Rupiah ke Dollar")
	fmt.Println("2. Rupiah ke Euro")
	fmt.Println("3. GBP ke Knut, Sickle, Galleon")
	fmt.Println("Masukan Nomor pilihan anda : ")
	fmt.Scan(&menu)

	if menu == 1 {
		IDR_USD()
	} else if menu == 2 {
		IDR_EUR()
	} else if menu == 3 {
		GBP_KNUT()
	} else {
		fmt.Println("Menu pilihan tidak tersedia")
	}
}

func IDR_EUR() {
	var IDR int
	var kurs float64 = 16892.74
	fmt.Println("Masukan Nilai Rupiah : ")
	fmt.Scan(&IDR)
	var convert float64 = float64(IDR) / kurs
	fmt.Println("Nilai Rupiah    = ", currency(float64(IDR), "IDR"))
	fmt.Println("Nilai Euro      = ", currency(convert, "EUR"))
	fmt.Println("Kurs            = ", currency(1, "EUR"), " -> ", currency(kurs, "IDR"))
}

func IDR_USD() {
	var IDR int
	var kurs float64 = 14231.75
	fmt.Println("Masukan Nilai Rupiah : ")
	fmt.Scan(&IDR)
	var convert float64 = float64(IDR) / kurs
	fmt.Println("Nilai Rupiah    = ", currency(float64(IDR), "IDR"))
	fmt.Println("Nilai US Dollar = ", currency(convert, "USD"))
	fmt.Println("Kurs            = ", currency(1, "USD"), " -> ", currency(kurs, "IDR"))
}

func GBP_KNUT() {
	var GBP int
	fmt.Println("Masukan Jumlah GB Pounds    : ")
	fmt.Scan(&GBP)
	var knut int = GBP * 100
	fmt.Println("Jumlah Knut yang didapat    = ", knut)
	var galleon int = (knut / 29) / 17
	var sisa_knut int = knut - ((galleon * 17) * 29)
	var sickle int = sisa_knut / 29
	sisa_knut = sisa_knut - (29 * sickle)
	fmt.Println("Hasil penukaran mendapatkan = ", galleon, " Galleon (s)")
	fmt.Println("Sisa ditukar menjadi        = ", sickle, " Sickle (s)")
	fmt.Println("Keping knut yang tersisa    = ", sisa_knut, " Knut (s)")
}

func currency(number float64, format string) string {
	// Modifikasi Dari tugas sebelumnya
	var money, sim string
	if format == "IDR" {
		money = "Rp. "
		sim = "."
	} else if format == "USD" {
		money = "$ "
		sim = ","
	} else if format == "EUR" {
		money = "€ "
		sim = ","
	} else {
		return "Error Number Format"
	}

	valnum := strings.Split(fmt.Sprintf("%.2f", number), ".")[0]
	var reverse string = ""

	var index int = 1

	for i := (strings.Count(valnum, "") - 2); i >= 0; i-- {
		if (index%3) == 1 && index != 1 {
			reverse += sim
		}
		reverse += string(valnum[i])

		index++
	}

	valnum = reverse

	for i := (strings.Count(valnum, "") - 2); i >= 0; i-- {
		money += string(valnum[i])
	}

	if format == "USD" {
		money += "." + strings.Split(fmt.Sprintf("%.2f", number), ".")[1] + " USD"
	} else if format == "IDR" {
		money += "," + strings.Split(fmt.Sprintf("%.2f", number), ".")[1]
	} else if format == "EUR" {
		money += "." + strings.Split(fmt.Sprintf("%.2f", number), ".")[1] + " EUR"
	}

	return money
}
